import math
import queue
import threading
from math import sqrt
import multiprocessing

import matplotlib.pyplot as plt

from LIAN_utils import Point, Node, getCircleSuccessors, dist, isTraversable, getAngle, draw_route_on_plot, draw_result, LineOfSight

TICK = 0

def Expand(a: Node, delta, alpha, goal: Node, image, OPEN, CLOSED):
    SUCC = getCircleSuccessors(a, delta)
    if dist(a, goal) < delta:
        SUCC.append(goal)

    xs = []
    ys = []

    for myA in SUCC:

        xs.append(myA.x)
        ys.append(myA.y)

        myA.parent = a
        # if not isTraversable(a, myA, image):
        #     continue

        if a.parent is not None:
            if getAngle(a.parent, a, myA) < alpha:
                continue

        if myA in CLOSED:
            continue

        if not LineOfSight(a, myA, image):
            continue

        myA.distance = a.distance + dist(a, myA)
        myA.finish_path = myA.distance + dist(myA, goal)
        # if TICK % 100 == 0:
        #     plt.plot(myA.x, myA.y, 'ro')
        # plt.text(myA.x, myA.y, str(str(myA.x) + " " + str(myA.y)), fontsize=10)
        OPEN.put(myA)

#    plt.plot(xs, ys)


def LIAN(start, goal, delta, alpha, image):
    OPEN = queue.PriorityQueue(maxsize=0)
    OPEN.put(start)
    CLOSED = set()

    while OPEN.qsize() > 0:
        global TICK
        print(f"TICK {TICK}")
        TICK += 1
        plt.imshow(image)
        # min_index = -1
        # min_val = 99999999999
        # for i in range(len(OPEN)):
        #     if OPEN[i].distance + dist(OPEN[i], goal) < min_val:
        #         min_index = i
        #         min_val = OPEN[i].distance + dist(OPEN[i], goal)
        As = []
        for i in range(1):
            if OPEN.qsize() == 0:
                break
            As.append(OPEN.get())
        if goal in As:
            draw_result(a, image)
            print("path found!")
            return

        for a in As:
            CLOSED.add(a)
        #
        # threads_list = list()
        # for a in As:
        #     t = threading.Thread(target=Expand, args=[a, delta, alpha, goal, image, OPEN, CLOSED])
        #     t.start()
        #     threads_list.append(t)

        if TICK % 100 == 0:
            for a in As:
                draw_route_on_plot(a, image)

        # for t in threads_list:
        #     t.join()

        Expand(a, delta, alpha, goal, image, OPEN, CLOSED)
        # if len(CLOSED) % 100 == 0:

        print(len(CLOSED), OPEN.qsize())

    print("no path found")
    return
