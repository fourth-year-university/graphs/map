import cv2
import matplotlib.pyplot as plt
import numpy as np
from LIAN import LIAN
from LIAN_utils import Node


if __name__ == "__main__":

    img = cv2.imread("map2.png")

    plt.ion()

    # START = Node(165, 305)
    # STOP = Node(1286, 689)
    START = Node(1, 1)
    STOP = Node(190, 190)

    DELTA = 20
    ALPHA = 25

    LIAN(START, STOP, DELTA, 180 - ALPHA, img)