from statistics import mean

import cv2
import numpy as np
import matplotlib.pyplot as plt


def find_coords_by_color(image, color):
    coords = np.where(image == color)
    print(len(coords[0]))
    return mean(coords[0]), mean(coords[1])


def cmp_colors(col1, stay):
    for col2 in stay:
        all_good = True
        for i in range(3):
            if col1[i] != col2[i]:
                all_good = False
        if all_good:
            return True
    return False



POINT_1 = [255, 201, 14]
POINT_2 = [237, 28, 36]
HOUSE = [0, 0, 0]

stay = [HOUSE]

img = cv2.imread("karta-01.bmp")
img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

img = np.array(img)

for i in range(len(img)):
    for j in range(len(img[i])):
        if not cmp_colors(img[i][j], stay):
            img[i][j] = np.array([255, 255, 255])

# print(find_coords_by_color(img, POINT_1))
# print(find_coords_by_color(img, POINT_2))

plt.imshow(img)
plt.show()
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

img_bin = []

for i in range(len(img)):
    p = []
    for j in range(len(img[i])):
        if img[i][j][0] == 0 and img[i][j][1] == 0 and img[i][j][2] == 0:
            p.append(1)
        else:
            p.append(0)
    img_bin.append(p)

plt.imshow(img_bin)
plt.show()

with open('test.npy', 'wb') as f:
    np.save(f, img_bin)


cv2.imwrite('map.png', img)
