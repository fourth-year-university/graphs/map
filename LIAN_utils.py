import math

import matplotlib.pyplot as plt
import numpy as np

from skimage.draw import line


class Point:
    x = 0
    y = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y


class Node(Point):
    parent = None
    distance = 0
    finish_path = 0

    def __init__(self, x, y, parent=None, distance=0):
        super().__init__(x, y)
        self.parent = parent
        self.distance = distance

    def __eq__(self, other):
        if other is None:
            return False
        if self.parent is None:
            return False
        return self.x == other.x and self.y == other.y and self.parent.x == other.parent.x and self.parent.y == other.parent.y

    def __hash__(self):
        return self.x * 10 ** 7 + self.y

    def __lt__(self, other):
        return self.finish_path < other.finish_path


def getCircleSuccessors(node: Node, delta: int):
    SUCC = []

    x = delta
    y = 0

    P = 1 - delta

    while x > y:

        y += 1

        if P <= 0:
            P = P + 2 * y + 1

        else:
            x -= 1
            P = P + 2 * y - 2 * x + 1

        if (x < y):
            break

        SUCC.append(Node(round(x + node.x), round(y + node.y)))
        SUCC.append(Node(round(-x + node.x), round(y + node.y)))
        SUCC.append(Node(round(x + node.x), round(-y + node.y)))
        SUCC.append(Node(round(-x + node.x), round(-y + node.y)))

        # If the generated point on the line x = y then
        # the perimeter points have already been printed
        if x != y:
            SUCC.append(Node(round(y + node.y), round(x + node.x)))
            SUCC.append(Node(round(-y + node.y), round(x + node.x)))
            SUCC.append(Node(round(y + node.y), round(-x + node.x)))
            SUCC.append(Node(round(-y + node.y), round(-x + node.x)))

    return SUCC


def dist(a: Point, b: Point):
    return ((a.x - b.x) ** 2 + (a.y - b.y) ** 2) ** 0.5


def isTraversable(a: Point, b: Point, image):
    n = max(abs(a.x - b.x), abs(a.y - b.y))
    xs = np.linspace(a.x, b.x, num=n)
    ys = np.linspace(a.y, b.y, num=n)

    for i in range(n):
        x = round(xs[i])
        y = round(ys[i])
        if x < 0 or x >= len(image[0]) or y < 0 or y >= len(image):
            return False
        if image[y][x][0] == 0 and image[y][x][1] == 0 and image[y][x][2] == 0:
            return False
    return True


def LineOfSight(a: Point, b: Point, image):
    rr, cc = line(a.x, b.x, a.y, b.y)
    for i in range(len(cc)):
        if rr[i] < 0 or rr[i] >= len(image[0]) or cc[i] < 0 or cc[i] >= len(image):
            return False
        if image[cc[i]][rr[i]][0] == 0 and image[cc[i]][rr[i]][1] == 0 and image[cc[i]][rr[i]][2] == 0:
            return False
    return True


def getAngle(a: Point, b: Point, c: Point):
    a1 = np.array([a.x, a.y])
    b1 = np.array([b.x, b.y])
    c1 = np.array([c.x, c.y])

    ba = a1 - b1
    bc = c1 - b1

    cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
    angle = math.degrees(np.arccos(cosine_angle))

    if np.isnan(angle):
        return 0
    return angle


def draw_route_on_plot(a: Node, image):
    xs = [a.x]
    ys = [a.y]
    while a.parent is not None:
        xs.append(a.parent.x)
        ys.append(a.parent.y)
        a = a.parent
    plt.plot(xs, ys)
    for i in range(len(xs)):
        plt.text(xs[i], ys[i], str(i), fontsize=10)
    plt.draw()
    plt.pause(0.00001)
    plt.clf()


def draw_result(a: Node, image):
    plt.imshow(image)
    xs = [a.x]
    ys = [a.y]
    while a.parent is not None:
        xs.append(a.parent.x)
        ys.append(a.parent.y)
        a = a.parent
    plt.plot(xs, ys)
    for i in range(len(xs)):
        plt.text(xs[i], ys[i], str(i), fontsize=10)
        if i > 0 and i < len(xs) - 1:
            print(
                f"point = {i}, deg = {getAngle(Point(xs[i - 1], ys[i - 1]), Point(xs[i], ys[i]), Point(xs[i + 1], ys[i + 1]))}")
    plt.show()
    plt.savefig('result.png')
